---
title: Keyboard shortcuts of the Polish Cookie Consent
---
Polish Cookie Consent uses [CodeMirror](https://codemirror.net/) library as an editor for entires.

Following keyboard shortcuts are available during editing:
{{< PCCShortcutsTable >}}
